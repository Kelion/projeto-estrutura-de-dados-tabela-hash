package Lista;

import util.Aluno;

public class ListaEncadeada extends Aluno {

    private static Aluno primeiro;
    private static Aluno ultimo;
    private int tamanho = 0;

    public ListaEncadeada() {
        this.primeiro = null;
        ultimo = primeiro;
    }

    public boolean isVoid(){

        if (primeiro == null) return true;

        return false;
    }

    public void addFim(int matricula, String nome, int idade){

        if(primeiro == null){

            primeiro = new Aluno();
            ultimo = primeiro;
            Aluno novo = new Aluno();

            novo.setMatricula(matricula);
            novo.setNome(nome);
            novo.setIdade(idade);

            novo.setProximo(null);
            ultimo.setProximo(novo);
            ultimo = novo;
            tamanho++;
        }

        else {

            Aluno novo = new Aluno();

            novo.setMatricula(matricula);
            novo.setNome(nome);
            novo.setIdade(idade);

            novo.setProximo(null);
            ultimo.setProximo(novo);
            ultimo = novo;
            tamanho++;
        }
    }

    public void print(){

        Aluno percorre = primeiro.getProximo();
        int aux = 1;

        if(isVoid()==true) {

            System.out.println("*** Lista Vazia ***");
        }
        else {

            while (percorre != null){
                System.out.println(aux+" - matricula - "+percorre.getMatricula());
                System.out.println("    nome - "+percorre.getNome());
                System.out.println("    idade - "+percorre.getIdade());
                System.out.println();
                percorre = percorre.getProximo();
                aux++;
            }
        }
    }

    public void remover(int matricula){
        Aluno remove = primeiro.getProximo();
        Aluno sentinela = primeiro; // Antecessor

        if(isVoid()){
            System.out.println("## Lista Vazia ##");
        }

        else{

            while(remove != null){

                if (remove.getMatricula() == matricula){

                    if (remove.getProximo() == null){

                        ultimo = sentinela;
                        ultimo.setProximo(null);
                        remove = null;
                        tamanho--;
                        System.out.println("## Removido com Sucesso ##");
                        break;

                    }else {

                        sentinela.setProximo(remove.getProximo());
                        remove.setProximo(null);
                        remove = null;
                        tamanho--;
                        System.out.println("## Removido com Sucesso ##");
                        break;
                    }
                }

                remove = remove.getProximo();
                sentinela = sentinela.getProximo();
            }
        }
    }

    public void busca(int matricula){

        Aluno percorre = primeiro.getProximo();
        int find = 0;

        while(percorre != null){
            if (percorre.getMatricula() == matricula){
                System.out.println("## O Aluno  foi encontrado ##");
                System.out.println("Matricula - "+percorre.getMatricula());
                System.out.println("Nome      - "+percorre.getNome());
                System.out.println("Idade     - "+percorre.getIdade());
                System.out.println();
                find = 1;
                break;
            }
            percorre = percorre.getProximo();
        }
        if (find != 1){
            System.out.println("## O Aluno não foi encontrado ##\n");
        }
    }
}
