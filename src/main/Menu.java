package main;

import hash.TabelaHashEncadeada;

import java.util.Scanner;

/**
 * @authors:
 * Jo�o Pedro Lima Vieria da Silva,
 * Kelion Fernandes Aquino,
 * Larissa Nahan Santos Diniz Gadelha Dantas
 */

public class Menu {

    public static void menu(){

            System.out.println("\n\n### Menu - Tabela Hash ###");
        System.out.println("\n                  =========================");
        System.out.println("                  |    1 - Inserir novo Aluno  |");
        System.out.println("                  |    2 - Buscar Aluno        |");
        System.out.println("                  |    3 - Remover Aluno       |");
        System.out.println("                  |    4 - Exibir Tabela Hash  |");
        System.out.println("                  |    0 - Encerrar Sistema    |");
        System.out.println("                  =========================\n");
    }

    public static void main(String[] args) {

        TabelaHashEncadeada hash = new TabelaHashEncadeada();
        Scanner console = new Scanner(System.in);
        int opcao;
        int matricula;

        do {
            menu();
            System.out.print("==>");
            opcao = console.nextInt();
            System.out.println();

            switch (opcao){
                case 0:

                    System.out.println("\n### This is all folks! ###");

                    break;

                case 1:

                    System.out.println("Insira a matricula do Aluno: ");
                    System.out.print("==>");
                    matricula = console.nextInt();
                    console.nextLine();

                    System.out.println("Insira o nome do Aluno: ");
                    System.out.print("==>");
                    String nome = console.nextLine();

                    System.out.println("Insira a idade do Aluno: ");
                    System.out.print("==>");
                    int idade = console.nextInt();
                    console.nextLine();

                    hash.addTabela(matricula, nome, idade);

                    break;

                case 2:

                    System.out.println("Insira a matricula do Aluno que você deseja buscar: ");
                    System.out.print("==>");
                    matricula = console.nextInt();

                    hash.buscaAluno(matricula);

                    break;

                case 3:

                    System.out.println("Insira a matricula do Aluno que você deseja remover: ");
                    System.out.print("==>");
                    matricula = console.nextInt();

                    hash.excluiAluno(matricula);

                    break;

                case 4:

                    System.out.println("Imprimindo Tabela...");

                    hash.imprime();

                    break;

                default:

                    System.out.println("\n### Choice must be a value between 1 and 4 ###");
            }

        }while(opcao != 0);

        console.close();
    }
}
