package hash;

import Lista.ListaEncadeada;

public class TabelaHashEncadeada {

    public ListaEncadeada vetor[];
    private final int CAPACIDADE_DEFAULT = 11;

    public TabelaHashEncadeada() {
        this.vetor = new ListaEncadeada[CAPACIDADE_DEFAULT];
        initTabela();
    }

    public void initTabela(){
        for (int i = 0; i < CAPACIDADE_DEFAULT; i++) {
            vetor[i] = new ListaEncadeada();
        }
    }

    public int hash(int matricula){
        return matricula % this.vetor.length;
    }

    public void addTabela(int matricula, String nome, int idade){
        vetor[hash(matricula)].addFim(matricula, nome, idade);
    }

    public void excluiAluno(int matricula){

        System.out.println("## Alunos nesse index: ##\n");
        vetor[hash(matricula)].print();
        vetor[hash(matricula)].remover(matricula);
        System.out.println("## Alunos nesse index: ##\n");
        vetor[hash(matricula)].print();
    }

    public void buscaAluno(int matricula){
        vetor[hash(matricula)].busca(matricula);
    }

    public void imprime(){

        for (int i = 0; i < CAPACIDADE_DEFAULT; i++) {

            vetor[i].print();
        }
    }



    public int getCAPACIDADE_DEFAULT() {
        return CAPACIDADE_DEFAULT;
    }

    public ListaEncadeada[] getVetor() {
        return vetor;
    }
}
